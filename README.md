# Docker for local PHP and NodeJS development/asset building

## Cloning Example Repo

```bash
git clone https://bitbucket.org/bemit_eu/docker-webdev.git
cd docker-webdev
```

### With Compose - Example Docker Hub Images

In the `example-hub` folder is a `docker-compose.yml` file for PHP and NodeJS and a Dockerfile only for NodeJS, this uses the images of this repo which are build on Docker hub.

```bash
cd ./example-hub

# start everything
docker-compose up
# or with rebuild 
docker-compose up --build

# Everything is running check:
# PHP: localhost:3000
# NodeJS: localhost:3001

# Run Commands in the started containers
docker-compose exec app_php bash
docker-compose exec app_node bash
# e.g. NPM install
docker-compose exec app_node npm install --save @insulo/runner

docker-compose down
```

### With Compose, Local Images

```bash
# first build base image for node app
docker build -t webdev-node ./node

# start everything
docker-compose up

# Shutdown
docker-compose down
```

### Only Docker

```bash
docker build -t webdev-node ./node

docker build -t webdev-node-app -f ./node/DockerFileApp ./app

docker build -t webdev-php -f ./php/DockerFile ./app

# /app must be absolute path to your project
docker run --name webdev-php --rm -i -t -v /app:/var/www/www -p 3000:8080 webdev-php bash

docker run --name webdev-node --rm -i -t -v /app:/var/www/www -p 3001:8081 webdev-node-app bash

# for starting PHP server in container on port 8080
./start.sh
# on host: localhost:3000

# for starting NodeJS server in container on port 8081
npm start
# on host: localhost:3001
```