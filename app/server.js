let express = require('express');
let app = express();

app.get('/', function(req, res) {
    console.log('Node serving server.js');
    res.send('Hello World!');
});

app.listen(8081, function() {
    console.log('Example app listening on port 8081!');
});
